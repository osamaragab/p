#include <stdio.h>
#include <stdlib.h>

#define PGDEF 22

static FILE *cons;
static int pglen = PGDEF;

static void printfile(FILE *f) {
	char buf[BUFSIZ] = {0};
	size_t len = 0;
	int c, pg = pglen;
	for (;;) {
		while ((c = fgetc(f)) != EOF) {
			if (c == '\n' && --pg == 0) {
				pg = pglen;
				break;
			}
			if (len+2 > BUFSIZ) {
				fputs(buf, stdout);
				for (buf[0] = 0; len > 0; buf[len--] = 0);
			}
			buf[len++] = c;
		}
		fputs(buf, stdout);
		fflush(stdout);
		for (buf[0] = 0; len > 0; buf[len--] = 0);
		if (c == EOF) {
			return;
		}
		if (fgetc(cons) == 'q') {
			exit(0);
		}
	}
	return;
}

int main(int argc, char *argv[]) {
	if ((cons = fopen("/dev/tty", "r")) == NULL) {
		fputs("p: can't open /dev/tty", stderr);
		return 1;
	}

	int n = 0;
	while (argc > 1) {
		argc--; argv++;
		if (*argv[0] == '-') {
			pglen = atoi(&argv[0][1]);
			if (pglen <= 0) {
				pglen = PGDEF;
			}
			continue;
		}
		n++;
		FILE *f = fopen(argv[0], "r");
		if (f == NULL) {
			fprintf(stderr, "p: can't open %s\n", argv[0]);
			continue;
		}
		printfile(f);
		fclose(f);
	}
	if (n == 0) {
		printfile(stdin);
	}

	return 0;
}
