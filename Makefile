CC = cc
CFLAGS = -std=c99 -pedantic -Wall -Wextra -O2

SRC = p.c
OBJ = $(SRC:.c=.o)
PREFIX = /usr/local

.c.o:
	$(CC) -c $(CFLAGS) $<

p: $(OBJ)
	$(CC) -o $@ $(OBJ)

clean:
	rm -f p $(OBJ)

install: p
	mkdir -p $(PREFIX)/bin
	cp -f p $(PREFIX)/bin
	chmod 755 $(PREFIX)/bin/p

uninstall:
	rm -f $(PREFIX)/bin/p

.PHONY: clean install uninstall
